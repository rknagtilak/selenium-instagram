
# Selenium Instgram Testing

Selenium Instgram follow public and private accounts.



## Installation

Step by Step Installation this project.

```bash
  python -m venv <venv_name>

  source <venv_name>/bin/activate

  git clone https://gitlab.com/rknagtilak/selenium-instagram.git 

  pip install -r requirements.txt

```
Finally done our setup.

You can add users list in public and private account username.

example :-
```
users = ['beingsalmankhan','rushikeshnagtilak']
```

Now You can run the program.
```
  python insta.py
```


    